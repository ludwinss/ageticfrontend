import * as React from 'react';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, List, ListItem, Typography, styled } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import InfoIcon from '@mui/icons-material/Info';
import LogoutIcon from '@mui/icons-material/Logout';
import { useSession } from '@/hooks/useSession';
import { useRouter } from 'next/router';

const HeaderStyled = styled(AppBar)({
    backgroundColor: 'var(--secondary)'
}
)

const ButtonStyled = styled(Button)({
    fontFamily: 'Oswald',
    fontWeight: 300,
    color: 'white',
    letterSpacing: '1px',
    fontSize: '1.1em',
    textTransform: 'none'
})

const ListStyled = styled(List)({
    display: 'flex',
    position: 'relative',
    marginLeft: 'auto',
    padding: 0,

})

const NavLinkStyled = styled(Button)({
    fontWeight: 300,
    fontSize: '.75em',
    letterSpacing: '1px',
    color: 'inherit',
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-start',
    '&:hover': {
        borderBottom: '2px solid white',
        borderRadius: 0
    }
})
const ListItemStyled = styled(ListItem)({
    padding: 0,
    display: 'flex',
    marginRight: '1.1em',
    marginLeft: '.75em',
    width: 'auto',
})
export function Header() {
    const router = useRouter()
    const [openDiag, setOpenDiag] = React.useState(false)
    const logout = () => {
        router.push('/')
    }
    const alert = () => {
        setOpenDiag(!openDiag)

    }
    return (
        <Box sx={{ flexGrow: 1 }}>
            <HeaderStyled position="static">
                <Toolbar>
                    <ButtonStyled>
                        AGETIC
                    </ButtonStyled>
                    <ListStyled>
                        <ListItemStyled>
                            <NavLinkStyled
                                startIcon={<InfoIcon />}
                                onClick={alert}
                            >
                                About
                            </NavLinkStyled>
                        </ListItemStyled>
                        <ListItemStyled >
                            <NavLinkStyled
                                startIcon={<LogoutIcon />}
                                onClick={logout}
                            >
                                LOG OUT
                            </NavLinkStyled>
                        </ListItemStyled>
                    </ListStyled>

                    <Dialog open={openDiag} onClose={alert}>
                        <DialogTitle>TIPOS DE USUARIO</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Existen dos tipos de cuentas.
                                <Typography variant='body1'>"Admin" puede hacer un CRUD completo (user:0001;pwd:asd)</Typography>
                                <Typography variant='body1'>"User" solamente puede ver la lista de productos(user:pepeelbueno;pwd:asd)</Typography>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={alert}>Close</Button>
                        </DialogActions>
                    </Dialog>
                </Toolbar>
            </HeaderStyled>
        </Box>
    );
}