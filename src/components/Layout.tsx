import { Box } from "@mui/material"
import { Header } from "./Header"
import { Footer } from "./Footer"

export function Layout({ children }: { children: any }) {
    return (
        <Box>
            <Header />
            <Box m={3} height={'100%'}>
                {children}
            </Box>
            <Footer />
        </Box>
    )
}