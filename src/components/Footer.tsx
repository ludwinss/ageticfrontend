import { styled, Box, Typography } from '@mui/material';

const FooterStyled = styled('footer')({
    width: '100%',
    backgroundColor: 'var(--footer)',
    boxShadow: '0px -1px 6px -1px rgba(0,0,0,0.75)',
    display: 'flex',
    justifyContent: 'space-around',
    bottom:0,
    position:'fixed'
})

export function Footer() {
    return (
        <FooterStyled>
            <Typography  fontWeight={300} fontSize={13} letterSpacing={3} color={'whitesmoke'}>
                ludwinssFM&copy;2023
            </Typography>
        </FooterStyled>
    )

}