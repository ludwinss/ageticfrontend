import { useSession } from '@/hooks/useSession';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Box, Typography, IconButton, styled, ButtonGroup, Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { getAllProduct } from '../api/product.service';
import { IProduct } from '@/interfaces/IProduct';
import { useAlert } from '@/hooks/useAlert';
import { Layout } from '@/components/Layout';
import { deleteProduct } from '../api/product.service';

import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import FormProduct from './FormProduct';

const IconButtonStyled = styled(IconButton)({
    color: 'var(--secondary)'
})
const ButtonStyled = styled(Button)({
    color: 'var(--secondary)',
    marginLeft: 'auto'
})
const columns = (typeUser: 'U' | 'A', setData: (type: IProduct) => void, handleModal: () => void, handleDialog: () => void): GridColDef[] => {
    const columnTemp: GridColDef[] = [
        { field: 'id', headerName: 'ID', width: 90 },
        {
            field: 'name',
            headerName: 'Product',
            width: 150,
        },
        {
            field: 'description',
            headerName: 'Descripcion',
            width: 150,
        },
        {
            field: 'category',
            headerName: 'Categoria',
            width: 110,
        }]
    if (typeUser === 'A') {
        columnTemp.push(
            {
                field: "",
                headerName: 'opciones',
                renderCell: (params) => {
                    return <ButtonGroup variant='outlined'>
                        <IconButtonStyled onClick={() => {
                            handleModal();
                            setData(params.row);
                        }}><EditIcon /></IconButtonStyled>
                        <IconButtonStyled onClick={() => {
                            handleDialog();
                            setData(params.row);
                        }}><DeleteIcon /></IconButtonStyled>
                    </ButtonGroup>
                }
            }
        )
    }
    return columnTemp;
};

function ProductPage() {
    const router = useRouter();
    const { sessionState } = useSession()
    const [productList, setProductList] = useState<IProduct[]>([])
    const [pickProduct, setPickProduct] = useState<IProduct>({} as IProduct)
    const [typeForm, setTypeForm] = useState<'Editar' | 'Añadir'>('Editar');
    const [openModal, setOpenModal] = useState(false)
    const [openDeleteDialog, setOpenDeleteDialog] = useState(false)
    const { alertDispatch } = useAlert();

    const verifiedLogin = () => {
        if (sessionState.token === '')
            return router.back()
    }
    const getProducts = async () => {
        const { response, auth } = await getAllProduct({ token: sessionState.token })
        if (auth && response && response.length > 0) return setProductList(response);
    }

    const handleModal = () => {
        getProducts();
        setOpenModal(!openModal);
        if (typeForm === "Añadir") { setTypeForm("Editar") }
    }
    const handleDialog = () => {
        setOpenDeleteDialog(!openDeleteDialog);
    }
    const deletePickProduct = async () => {
        try {
            const response = await deleteProduct({ token: sessionState.token, id: pickProduct.id as any })
            if (!response.auth) throw new Error(response.response)
            alertDispatch({ type: 'success', message: 'Eliminado exitosamente' })
        } catch (error: any) {
            alertDispatch({ type: 'error', message: String(error.message || error) })
        }
        handleDialog();
        getProducts()
    }
    const editPickProduct = (product: IProduct) => { setPickProduct(product) };

    const addProduct = () => {
        setTypeForm('Añadir');
        setPickProduct({} as IProduct)
        setOpenModal(true);
    }

    useEffect(() => {
        verifiedLogin()
    }, [sessionState])

    useEffect(() => {
        getProducts()
    }, [])

    // console.log(sessionState)
    return (
        <Layout>
            <Box bgcolor={'white'} p={2}>

                <Typography variant='h5' marginY={3} > Lista de Productos </Typography>
                {sessionState.perfil === 'A' && <ButtonStyled startIcon={<AddIcon />} onClick={addProduct}>Añadir Producto</ButtonStyled>}

                <DataGrid columns={columns(sessionState.perfil, editPickProduct, handleModal, handleDialog)} rows={productList} pageSizeOptions={[6]}
                    initialState={{
                        pagination: {
                            paginationModel: {
                                pageSize: 6,
                            },
                        },
                    }}
                />
                {openModal && <FormProduct form={pickProduct} open={openModal} handleOpen={handleModal} type={typeForm} />}

                {openDeleteDialog &&
                    <Dialog
                        open={openDeleteDialog}
                        onClose={handleDialog}
                    >
                        <DialogTitle >
                            {"Desea Eliminar el Producto?"}
                        </DialogTitle>
                        <DialogActions>
                            <Button onClick={handleDialog}>No</Button>
                            <Button onClick={deletePickProduct} autoFocus>
                                Estoy Deacuerdo
                            </Button>
                        </DialogActions>
                    </Dialog>}

            </Box>
        </Layout>
    );
}
export default ProductPage;
