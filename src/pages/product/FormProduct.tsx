import { useAlert } from "@/hooks/useAlert";
import { IProduct } from "@/interfaces/IProduct";
import { Button, CircularProgress, IconButton, Modal, TextField, Typography, styled } from "@mui/material";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";

import CloseIcon from '@mui/icons-material/Close'
import moment from "moment";
import { getEditProduct, postNewProduct } from "../api/product.service";
import { useSession } from "@/hooks/useSession";

const FormStyled = styled('form')(({ theme }) => ({
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'space-evenly',
    backgroundColor: 'white',
    padding: theme.spacing(3),
    boxShadow: theme.shadows[2],
    minWidth: 'var(--global-width)',
    minHeight: 'var(--global-height)',
}))

const ButtonStyled = styled(Button)({
    backgroundColor: "var(--secondary)",
    color: 'white',
    ':hover': {
        color: 'var(--secondary)'
    }
})

export default function FormProduct({ form, type, open, handleOpen }: { form: IProduct, type: 'Editar' | 'Añadir', open: boolean, handleOpen: () => void }) {
    const { alertDispatch } = useAlert();
    const { sessionState } = useSession()
    const initValues: { name: string, category: string, description: string, create_at: string } = {
        name: form.name || '',
        category: form.category || '',
        description: form.description || '',
        create_at: moment(form.created_at || moment.now()).format('L')
    }
    const { control, handleSubmit, formState: { errors } } = useForm({ defaultValues: initValues })

    const submit = async (data: typeof initValues) => {
        setLoading(true);
        try {
            if (type === "Editar") {
                const response = await getEditProduct({ token: sessionState.token, form: { ...data, id: form.id } as any })
                if (!response.auth) throw new Error(response.response)
                alertDispatch({ type: 'success', message: 'Editado exitosamente' })
            }
            if (type === "Añadir") {
                const response = await postNewProduct({ token: sessionState.token, form: { ...data } as any })
                if (!response.auth) throw new Error(response.response)
                alertDispatch({ type: 'success', message: 'Editado exitosamente' })

            }
        } catch (error: any) {
            alertDispatch({ type: 'error', message: String(error.message || error) })
        }
            handleOpen()
        setLoading(false);
    }

    const [loadingForm, setLoading] = useState(false);

    return (
        <Modal open={open} onClose={handleOpen} sx={{ top: 'auto' }}>
            <FormStyled onSubmit={handleSubmit(submit)}>
                <div style={{ alignItems: 'center', display: 'flex', justifyContent: 'space-between' }}>
                    <IconButton onClick={handleOpen}><CloseIcon /></IconButton>
                    <Typography variant='h6' textAlign={'right'}>{type} Product </Typography>
                </div>
                <div style={{ display: 'flex', gap: '1rem', flexDirection: 'column' }}>
                    <Controller
                        name='name'
                        control={control}
                        rules={{
                            required: 'Name is mandatory',
                        }}
                        render={({ field }) => <TextField {...field} variant="outlined" label="Nombre Producto" inputProps={{ maxLength: 100 }} error={Boolean(errors.name?.message)} helperText={errors.name?.message} />}
                    />
                    <Controller
                        name='description'
                        rules={{
                            required: 'description is mandatory',
                        }}
                        control={control}
                        render={({ field }) => <TextField {...field} variant="outlined" label="Descripcion" multiline inputProps={{ maxLength: 200 }} error={Boolean(errors.description?.message)} helperText={errors.description?.message} />}
                    />

                    <Controller
                        name='category'
                        rules={{
                            required: 'category is mandatory',
                        }}
                        control={control}
                        render={({ field }) => <TextField {...field} variant="outlined" label="Categoria" inputProps={{ maxLength: 200 }} error={Boolean(errors.category?.message)} helperText={errors.category?.message} />}
                    />
                    {type === 'Editar' && <Controller
                        name='create_at'
                        control={control}
                        render={({ field }) => <TextField {...field} variant="outlined" label="Fecha Creacion" inputProps={{ maxLength: 200 }} disabled />}
                    />}
                    {!loadingForm ? <ButtonStyled type='submit'>{type}</ButtonStyled> : <CircularProgress sx={{ alignSelf: 'center' }} />}

                </div>
            </FormStyled>
        </Modal>
    )
}