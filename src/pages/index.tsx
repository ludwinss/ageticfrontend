
import styles from '../styles/Home.module.css'
import FormLogin from "../components/FormLogin";
import { Grid } from "@mui/material";
import Head from 'next/head';

export default function Home() {
  return (
    <>
      <Head>
      </Head>
      <div className={styles.container}>
        <Grid container justifyContent='center' alignItems='center'>
          <Grid item>
            <FormLogin title='AGETIC' />
          </Grid>
        </Grid>
      </div>

    </>
  )
}
