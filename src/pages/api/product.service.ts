import { IProduct } from "@/interfaces/IProduct";

const HOST = `http://localhost:3333`

export const getAllProduct = (data: { token: string }): Promise<{ auth: boolean, response: any }> => {
    if (!HOST) throw new Error('.ENV doesnt provider');

    return fetch(`${HOST}/api/product/all`, {
        method: "GET", headers: {
            "Authorization": `Bearer ${data.token}`
        }
    })
        .then(response => {
            if (response.status !== 200) throw new Error("Product dont Found");
            return response.json()
        })
        .then(response => {
            return { auth: true, response: response }
        }).catch(error => {
            return { auth: false, response: error }
        }
        )
}
export const getEditProduct = (data: { token: string, form: IProduct }): Promise<{ auth: boolean, response: any }> => {
    if (!HOST) throw new Error('.ENV doesnt provider');

    return fetch(`${HOST}/api/product/${data.form.id}`, {
        method: "post", 
        headers: {
            "Authorization": `Bearer ${data.token}`
        }, body: new URLSearchParams({name:data.form.name,category:data.form.category,description:data.form.description}),
    },)
        .then(response => {
            console.log(response)
            if (response.status !== 201) throw new Error("Error Edit Product");
            return response.json()
        })
        .then(response => {
            return { auth: true, response: response }
        }).catch(error => {
            return { auth: false, response: error }
        }
        )
}
export const postNewProduct = (data: { token: string, form: IProduct }): Promise<{ auth: boolean, response: any }> => {
    if (!HOST) throw new Error('.ENV doesnt provider');

    return fetch(`${HOST}/api/product/create`, {
        method: "post", 
        headers: {
            "Authorization": `Bearer ${data.token}`
        }, body: new URLSearchParams({name:data.form.name,category:data.form.category,description:data.form.description}),
    },)
        .then(response => {
            console.log(response)
            if (response.status !== 201) throw new Error("Error Create Product");
            return response.json()
        })
        .then(response => {
            return { auth: true, response: response }
        }).catch(error => {
            return { auth: false, response: error }
        }
        )
}
export const deleteProduct = (data: { token: string, id: number }): Promise<{ auth: boolean, response: any }> => {
    if (!HOST) throw new Error('.ENV doesnt provider');

    return fetch(`${HOST}/api/product/${data.id}`, {
        method: "delete", 
        headers: {
            "Authorization": `Bearer ${data.token}`
        }, 
    },)
        .then(response => {
            console.log(response)
            if (response.status !== 200) throw new Error("Error Elimnate Product");
            return response.json()
        })
        .then(response => {
            return { auth: true, response: response }
        }).catch(error => {
            return { auth: false, response: error }
        }
        )
}