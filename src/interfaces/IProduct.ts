export interface IProduct {
    id: string,
    name: string,
    description: string,
    category: string,
    created_at: string,
}